﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TP.Security.API.Model {

       
    public class TicketAnonymous
    {
        [JsonProperty("subject")]
        public string Subject { get; set; }

        [JsonProperty("comment")]
        public TicketComments Comment { get; set; }

        [JsonProperty("requester")]
        public TicketRequesters Requester { get; set; }       
    }

    public class TicketComments {
        [JsonProperty("body")]
        public string Body { get; set; }
    }

    public class TicketRequesters
    {
        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public class TicketRequest {

        [JsonProperty("request")]
        public TicketAnonymous ticket{get;set;}
    }

}
