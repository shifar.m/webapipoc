﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace TP.Security.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddMvc();
            services.AddCors(
                 options => options.AddPolicy("AllowCors",
     builder => {
         builder
           .AllowAnyOrigin()
           .WithMethods("GET", "PUT", "POST", "DELETE")
           .AllowAnyHeader(); //AllowAllHeaders;  
     })
  );

            HttpClient httpClient = new HttpClient();
            services.AddSingleton<HttpClient>(httpClient); // note the singleton
            services.AddMvcCore()
               .AddAuthorization()
               .AddJsonFormatters();
            services.AddLocalization(o => o.ResourcesPath = "ApplicationConstants");

            services.AddAuthentication("Bearer")
                .AddIdentityServerAuthentication(options =>
                {
                    options.Authority = "http://localhost:5000"; //// Auth server
                    options.RequireHttpsMetadata = false;

                    options.ApiName = "api1";  //// API Name
                });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            //Enable CORS policy "AllowCors"
            app.UseCors("AllowCors");

            app.UseAuthentication();

            app.UseMvc();
        }
    }
}
