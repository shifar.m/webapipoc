﻿
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace TP.Security.API.Controllers
{
    /// <summary>
    /// Used to get Identity
    /// </summary>
    [Route("[controller]")]
    [Authorize]    
    public class IdentityController : Controller
    {
        /// <summary>
        /// controller for getting Identity
        /// </summary>
        /// <returns>User.Claims as json</returns>
        [HttpGet]
        public IActionResult Get()
        {
            return new JsonResult(from c in User.Claims select new { c.Type, c.Value });
        }
    }
}
