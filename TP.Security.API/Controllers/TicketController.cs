﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TP.Security.API.Model;
using System.Resources;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using System.Net;
using TP.Security.API.Resources;

namespace TP.Security.API.Controllers
{
    /// <summary>
    /// To access zendesk tickets
    /// </summary>
    [Route("api/ticket")]   
    public class TicketController : Controller
    {
        #region Variable
        /// <summary>
        /// declare a HttpClient
        /// </summary>
        private readonly HttpClient httpClient;
        #endregion Variable

        #region Constructor
        public TicketController(HttpClient HttpClient) // this is the singelton instance of HttpClient
        {
            httpClient = HttpClient; // assign it to the local HttpClient
        }
        #endregion Constructor


        [HttpGet]
        public String Get()
        {
            return "API hosted successfully ! ";
        }

        /// <summary>
        /// used to create ticket
        /// </summary>
        /// <param name="ticketRequest"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        [Route("newticket")]
        public async Task<IActionResult> CreateTicket([FromBody]TicketRequest ticketRequest)
        {
            try
            {
                string hosturl = AppConstants.ZendeskPostUrl;            

                // Serialize our concrete class into a JSON String
                var stringPayload = await Task.Run(() => JsonConvert.SerializeObject(ticketRequest));               

                var httpContent = new StringContent(stringPayload, Encoding.UTF8, AppConstants.ContentType);

                    // Do the actual request and await the response
                    var httpResponse = await httpClient.PostAsync(hosturl, httpContent);

                    // If the response contains content we want to read it!
                    if (httpResponse.Content != null)
                    {
                        var responseContent = await httpResponse.Content.ReadAsStringAsync();
                        JObject results = JObject.Parse(responseContent);
                        string Id = (string)results["request"]["id"];
                        string Subject = (string)results["request"]["subject"];
                        string Description = (string)results["request"]["description"];
                        string Status = (string)results["request"]["status"];
                        string Created = (string)results["request"]["created_at"];                        
                        string Updated = (string)results["request"]["updated_at"];
                        var response = new
                        {
                            Id=Id,
                            Subject = Subject,
                            Description = Description,
                            Status = Status,
                            Created= Created,
                            Updated = Updated
                        };
                        return Ok(JsonConvert.SerializeObject(response));                       
                    }
                    else
                        return NotFound();
            }
            catch (Exception ex)
            {
                return NotFound();
            }            
           
        }

        /// <summary>
        /// used to get ticket details by id
        /// </summary>
        /// <param name="ticketRequest"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        [Route("getticket")]
        public async Task<IActionResult> GetTicket([FromQuery]String ticket)
        {
            try
            {
                string hosturl = AppConstants.ZendeskGetUrl+ ticket + ".json";               
               
                     httpClient.DefaultRequestHeaders.Authorization
                        = new AuthenticationHeaderValue("Basic", AppConstants.ZendeskToken);
                    
                    // Do the actual request and await the response
                    var httpResponse = await httpClient.GetAsync(hosturl);
                   
                    // If the response contains content we want to read it!
                    if (httpResponse.Content != null)
                    {
                        var responseContent = await httpResponse.Content.ReadAsStringAsync();

                       
                        JObject results = JObject.Parse(responseContent);

                        string Subject = (string)results["ticket"]["subject"];
                        string Description = (string)results["ticket"]["description"];
                        string Status = (string)results["ticket"]["status"];                       
                        string Updated = (string)results["ticket"]["updated_at"];

                        var response = new
                        {
                            Subject = Subject,
                            Description = Description,
                            Status= Status,
                            Updated= Updated
                        };

                        return Ok(JsonConvert.SerializeObject(response));
                      
                    }
                    else
                        return NotFound();

            }
            catch (Exception ex)
            {
                return NotFound();
            }          
        }
       
    }
}